var station = module.exports,
    stationService = require('../services/stationService');

station.getList = function (context) {
    stationService.getAllWithFirstTrack(function (model) {
        context.json(model);
    });
};

station.getInfo = function (context) {
    stationService.getById(context.data.id, function (model) {
        context.json(model);
    });
};

station.getPlay = function(context){
    var stationId = context.data.id,
        timestamp = context.data.stamp;
    stationService.getStreamUrl(stationId, timestamp || 'now', function(url){
        context.response.writeHead(302, {location: url});
        context.response.end();
    });

};