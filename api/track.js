var track = module.exports,
    vk = require('vk'),
    Url = require('url'),
    path = require('path'),
    request = require('request');

track.getLinks = function (context) {
    vk.findTracks(context.data.q, function (tracks) {
        tracks = tracks.map(function (track) {
            var url = Url.parse(track.url),
                ext = path.extname(url.pathname);
            track.url = '/api/track/download?url=' + encodeURIComponent(url.protocol + '//' + url.host + url.path) + '&name=' + encodeURIComponent(track.artist + ' ' + track.title + ext);
            return track;
        });
        context.json(tracks);
    });
};

track.getDownload = function (context) {
    var url = context.data.url,
        filename = context.data.name;

    context.response.setHeader('Content-disposition', 'attachment; filename=' + filename);

    //todo: local cache
    request.get(url).pipe(context.response);
};