var express = require('express'),
    http = require('http'),
    path = require('path'),
    domain = require("domain"),
    mongoose = require('mongoose'),
    models = require('models'),
    router = require('router'),
    config = require('config');

var app = express(),
    mongo = config.mongo;

mongoose.connect('mongodb://' + mongo.username + ':' + mongo.pass + '@' + mongo.host + ':' + mongo.port + '/' + mongo.dbname);

var db = mongoose.connection;

db.on('error', function (err) {
    console.log('Conn error:', err.message);
});
db.once('open', function callback() {
    console.log("Connection to db established");
});

models.createModels();

app.configure("all", function () {
    app.set('port', process.env.PORT || 3000);
    app.set('views', path.join(__dirname, 'views'));
    app.set("view engine", "jade");
    app.use(express.logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded());
    app.use(express.methodOverride());
    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.session({secret: "alalalala"}));
});

app.all("*", function (req, res, next) {
    router.route(req, res, next);
});

app.configure("all", function () {
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
});

var httpServer = http.createServer(function (req, res) {
    var dom = domain.create();
    dom.add(req);
    dom.add(res);
    res.on("close", function () {
        dom.dispose();
    });
    dom.on("error", function (error) {
        try {
            res.send(500, "Server Error");
        } catch (err) {
            console.error("Domain second error", err);
        }
        dom.dispose();
    });
    dom.run(function () {
        app(req, res);
    });
});
httpServer.on("error", function (error) {
    if (error.code != "EADDRINUSE") return;
    setTimeout(function () {
        httpServer.close();
        httpServer.listen(port);
    }, 1000);
});
httpServer.listen(3000);