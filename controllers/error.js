var error = module.exports;

error["404"] = function (context) {
    context.send({ title: '404 - Not found' });
}