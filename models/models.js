var mongoose = require("mongoose"),
    station = require('./station'),
    track = require('./track');

exports.createModels = function() {
    station.create();
    track.create();
};