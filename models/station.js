var mongoose = require("mongoose");

exports.create = function () {
    var stationSchema = new mongoose.Schema({
        nid:  { type: String, required: true },
        sid:  { type: String, required: true },
        name:  { type: String, required: true },
        radioHref: { type: String, required: true },
        streamUrl: { type: String, required: true }
    }, { autoIndex: false });

    stationSchema.pre('save', function (next, done) {
        mongoose.models['Station'].findOne({
            nid: this.nid
        }, function (err, station) {
            if (err) {
                console.log('count err: ' + err);
                done(err);
            }
            else {
                if (!station) {
                    next();
                }
                else {
                    done({ continue: true, message: "Don't pass unique validation", stationId: station._id });
                }
            }
        });
    });

    mongoose.model('Station', stationSchema);
};