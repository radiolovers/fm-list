var mongoose = require("mongoose");

exports.create = function () {
    var trackSchema = new mongoose.Schema({
        artistName:  { type: String, required: true },
        trackName:  { type: String, required: true },
        timeStamp:  { type: Number, required: true },
        duration: { type: Number, required: true },
        stationId: { type: mongoose.Schema.ObjectId, required: true}
    }, { autoIndex: false });

    trackSchema.pre('save', function (next, done) {
        var tracks = mongoose.models['Track'],
            query = {
                $and: [
                    { stationId: this.stationId },
                    { timeStamp: this.timeStamp }
                ]
            };
        tracks.update(query, { duration: this.duration }, function (err, count, raw) {
            if (err) {
                console.log('update track err: ' + err);
            }
            else if (count === 0) {
                next();
                return;
            }
            done(err);
        });
    });

    mongoose.model('Track', trackSchema);
};