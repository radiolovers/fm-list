'use strict';

var app = angular.module('fmApp', ['ui.router']).
    config(function($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('stations', {
                url: '/station/',
                templateUrl: '/partials/station-list.html',
                controller: 'StationListCtrl'
            })
            .state('stations.detail', {
                url: ':id/',
                templateUrl: '/partials/station-info.html',
                controller: 'StationCtrl'
            });

        $urlRouterProvider.otherwise("/station/");
        $locationProvider.html5Mode(true);
    });