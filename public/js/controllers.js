'use strict';

app.controller('StationCtrl', function ($scope, $stateParams, $timeout, StationService, TrackService) {
    StationService.getInfo($stateParams.id).then(function (res) {
        $scope.station = res;
    });
    $scope.copyToClipboard = function (s) {
        if (window.clipboardData && clipboardData.setData) {
            clipboardData.setData('text', s);
        }
    };
    $scope.downloadTrack = function (q) {
        TrackService.getLinks(q).then(function (links) {
            console.log(links);
            if (links.length) {
                var link = document.createElement("a");
                link.download = q + '.mp3';
                link.href = links[0].url;
                link.click();
            }
        });
    };

    $scope.allowRefresh = true;

    var refreshInfo = function(callback){
        StationService.getInfo($stateParams.id).then(function (res) {
            $scope.station = res;
            console.log('station info refresh complete');
            callback();
        }, callback);
    };
    var loopRefresh = function(){
        if (!$scope.allowRefresh) return;
        $timeout(function () {
            refreshInfo(loopRefresh);
        }, 15000);
    };
    loopRefresh();
    $scope.$on('$destroy', function() {
        $scope.allowRefresh = false;
    });
});

app.controller('StationListCtrl', function ($scope, $timeout, StationService) {
    $scope.stations = StationService.getList();

    $scope.allowRefresh = true;

    var refreshList = function(callback){
        StationService.getList().$$promise.then(function (res) {
            $scope.stations = res;
            console.log('stations refresh complete');
            callback();
        }, callback);
    };
    var loopRefresh = function(){
        if (!$scope.allowRefresh) return;
        $timeout(function () {
            refreshList(loopRefresh);
        }, 15000);
    };
    loopRefresh();

    $scope.$on('$destroy', function() {
        $scope.allowRefresh = false;
    });
});