var isFlashDisabled = false;

app.directive('clipboard', function () {

    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element) {
            if (isFlashDisabled){
                element.css('display', 'none');
                return;
            }
            var clip = new ZeroClipboard(element);

            clip.on("ready", function () {
                console.log("Flash movie loaded and ready.");

                this.on("aftercopy", function (event) {
                    console.log("Copied text to clipboard: " + event.data["text/plain"]);
                });
            });

            clip.on("error", function (event) {
                isFlashDisabled = true;
                element.css('display', 'none');
                console.log('error[name="' + event.name + '"]: ' + event.message);
                ZeroClipboard.destroy();
            });
        }
    }

});