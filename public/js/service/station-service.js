
app.service('StationService', function($http){

    var service = {

        getList: function(){
            var result = [];
            result.$$promise = $http.get('/api/station/list').then(function(res){
                res.data.forEach(function(el){
                    result.push(el);
                });
                return result;
            });
            return result;
        },

        getInfo: function(id){
            return $http.get('/api/station/info', {params: {id: id}}).then(function(res){
                return res.data.station;
            });
        }

    };

    return service;
});
