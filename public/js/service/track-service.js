
app.service('TrackService', function($http){

    var service = {

        getLinks: function(q){
            return $http.get('/api/track/links', {params: {q: q}}).then(function(res){
                return res.data;
            });
        }

    };

    return service;
});
