var moskva = require('moskvafm'),
    models = require('models'),
    async = require('async'),
    mongoose = require('mongoose'),
    config = require('config');

var mongo = config.mongo;

mongoose.connect('mongodb://' + mongo.username + ':' + mongo.pass + '@' + mongo.host + ':' + mongo.port + '/' + mongo.dbname);

var db = mongoose.connection;

db.on('error', function (err) {
    console.log('Conn error:', err.message);
});
db.once('open', function callback() {
    console.log("Connection to db established");
});

models.createModels();

moskva.getStations(function (err, stations) {
    if (err) {
        console.log(err);
    }
    else {
        console.log('saving');
        var stationModel = mongoose.models['Station'],
            trackModel = mongoose.models['Track'];
        async.each(stations, function (item, statCb) {
            if (!item) {
                return statCb();
            }
            var model = new stationModel({
                nid: item.nid,
                sid: item.sid,
                name: item.name,
                radioHref: 'www.moskva.fm',
                streamUrl: item.streamUrl
            });
            model.save(function (err) {
                var stationId;
                if (err && err.continue) {
                    stationId = err.stationId;
                    err = null;
                }
                stationId = stationId || model._id;
                async.each(item.tracks, function (track, trackCb) {
                    var trModel = new trackModel({
                        artistName:  track.artistName,
                        trackName:  track.trackName,
                        timeStamp:  track.timeStamp,
                        duration: track.duration,
                        stationId: stationId
                    });
                    trModel.save(function (err) {
                        if (err && err.continue) {
                            err = null;
                        }
                        trackCb(err);
                    });
                }, function (err) {
                    statCb(err);
                });
            });
        }, function (err) {
            if (err) {
                console.log(err);
            }
            console.log('done');
            process.exit(0);
        });
    }
});