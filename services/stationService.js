var service = module.exports,
    config = require('config'),
    mongoose = require('mongoose'),
    async = require('async'),
    stations = mongoose.models['Station'],
    tracks =  mongoose.models['Track'],
    utils = require('utils'),
    offsetSeconds = 4 * 3600;

service.getAllWithFirstTrack = function (cb) {
    stations.find({}, function (err, stations) {
        var mappedStations = [];
        async.each(stations, function (st, cb) {
            tracks.find({ stationId: st._id }).sort({ timeStamp: -1 }).limit(1).exec(function (err, track) {
                var first = track && track.length > 0 ? track[0] : null,
                    timeStamp = /*first ? first.timeStamp : */Math.ceil(Date.now() / 1000);
                mappedStations.push({
                    id: st._id,
                    streamUrl: st.streamUrl.replace('{id}', st.nid).replace('{stamp}', timeStamp + offsetSeconds - 150),
                    name: st.name,
                    currentTrack: first ? {
                        artistName:  first.artistName,
                        trackName:  first.trackName,
                        timeStamp:  timeStamp,
                        duration: first.duration
                    } : null
                });
                cb(err);
            });
        }, function (err) {
            if (err) {
                console.log(err);
            }
            else {
                cb(mappedStations.sort(utils.sortstring));
            }
        });
    });
};

service.getById = function (id, cb) {
    stations.findOne({ _id: id }, function (err, station) {
        if (!station) {
            return cb({});
        }

        tracks.find({ stationId: station._id }).sort({ timeStamp: -1 }).limit(30).exec(function (err, tracks) {
            var model = {
                title: station.name,
                streamUrl: getStationPlayUrl(id, 'now'),
                tracks: tracks.map(function (track) {
                        return {
                            artistName:  track.artistName,
                            trackName:  track.trackName,
                            timeStamp:  1000 * (track.timeStamp),
                            duration: track.duration,
                            streamUrl: getStationPlayUrl(id, track.timeStamp + offsetSeconds)
                        };
                    })
            };
            cb({ station: model });
        });
    });
};

service.getStreamUrl = function(id, stamp, cb){
    stations.findOne({ _id: id }, function (err, station) {
        if (!station) {
            return cb('');
        }
        tracks.find({ stationId: station._id }).sort({ timeStamp: -1 }).limit(1).exec(function (err, track) {
            if (!stamp || stamp === 'now'){
                stamp = track ? (track.timeStamp + offsetSeconds + track.duration - 50) : (Math.ceil(Date.now() / 1000) + offsetSeconds - 150);
            }
            var url = station.streamUrl.replace('{id}', station.nid).replace('{stamp}', stamp);
            return cb(url);
        });
    });
};

var getStationPlayUrl = function(id, timestamp){
    return config.server.host + '/api/station/play?id=' + encodeURIComponent(id) + '&stamp=' + encodeURIComponent('' + timestamp);
};